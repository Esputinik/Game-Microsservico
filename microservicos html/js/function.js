let criarJogador = document.querySelector("#criarJogador");
let label_idjogador = document.querySelector("#label_idjogador");
let label_jogador = document.querySelector("#label_jogador");
let selectJogos = document.querySelector("#jogos_criados");
let listarjogos = document.querySelector("#list_jogos_criados");
let novoGame = document.querySelector("#novoGame");
let numeroQuestoes = document.querySelector("#numeroQuestoes");
let criarJogo = document.querySelector("#criarJogo");
let jogarJogo = document.querySelector("#jogarJogo");

let descricaoPergunta = document.querySelector("#descricaoPergunta");
let rb_p1 = document.querySelector("#lrb_p1");
let rb_p2 = document.querySelector("#lrb_p2");
let rb_p3 = document.querySelector("#lrb_p3");
let rb_p4 = document.querySelector("#lrb_p4");

let fimRanking = document.querySelector("#fimRanking");
let ranking = document.querySelector("#ranking");


var jsonExtract = function (httpResponse){
    return httpResponse.json();
}

function post(endpoint, data){

    return fetch(endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json'
        }
    }).then(jsonExtract).then((response) => {
        return response;
    });
}

function get(endpoint){

    return fetch(endpoint,
    {
        method: 'get',
        headers: {
            'content-type': 'application/json'
        }
    }).then(jsonExtract).then((response) => {
        return response;
    });
}


criarJogador.onclick = () => {
    let dadosCadPlayer = {
       name : document.querySelector("#jogador").value
    }
    post('http://10.162.110.84:8001/player/',dadosCadPlayer).then(dadosCadPlayerReponse);
}


function dadosCadPlayerReponse (response){
    label_jogador.innerHTML = "Seja Bem Vindo, " + response.name;
    label_idjogador.innerHTML = "Id, " + response.idPlayer;
}

function preencherJogos (response){

	while (selectJogos.lastChild) {
	  selectJogos.removeChild(selectJogos.lastChild);
	}

    opcao = document.createElement("option");
    opcao.innerHTML = "Selecione..";
    opcao.style = "selected"
    selectJogos.appendChild(opcao);

    for(let i=0; i < response.length; i++){
        opcao = document.createElement("option");
        opcao.value = response[i].idGame;
        opcao.innerHTML = response[i].name;
        selectJogos.appendChild(opcao);
    }
}

listarjogos.onclick = () => {
	get('http://10.162.108.239:8000/game/all').then(preencherJogos);
}

criarJogo.onclick = () => {
   let dadosCadGame = {
       name : novoGame.value,
       numberOfQuestions : numeroQuestoes.value
    }
    post('http://10.162.108.239:8000/game/new',dadosCadGame).then(preencherJogos);
}

function mostrarDados (response){
	alert(response);
	console.log(response);
}

jogarJogo.onclick = () => {
	let dadosCadGame = {
       idGame : selectJogos.value,
       idPlayer : label_idjogador.value
    }
    post('http://10.162.108.239:8001/gameexecution/play').then(mostrarDados);
    //carregarPergunta();
}

function carregarPergunta(pergunta){
	url = 'http://10.162.110.210:8080/question/' + 1;

	get(url).then((response) => {
		descricaoPergunta.innerHTML = response.title;
		rb_p1.innerHTML = response.option1;
		rb_p2.innerHTML = response.option2;
		rb_p3.innerHTML = response.option3;
		rb_p4.innerHTML = response.option4;
    });
}

fimRanking.onclick = () => {
	url = 'http://10.162.110.25:8002/ranking/' + 1;

	get(url).then((response) => {

		console.log(response);

		for (i=0;i< response.length;i++){
		item = document.createElement("label");
	    item.innerHTML = "Jogador: " + response[i].idPlayer + " | Pontos: " + response[i].pontos + "<br>";
		ranking.appendChild(item);			
		}
    });
}