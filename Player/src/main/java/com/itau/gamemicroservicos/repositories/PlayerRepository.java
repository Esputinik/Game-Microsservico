package com.itau.gamemicroservicos.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.gamemicroservicos.models.Player;

public interface PlayerRepository extends CrudRepository<Player, Integer> {

	public Optional<Player> findByidPlayer(int idPlayer);

}
