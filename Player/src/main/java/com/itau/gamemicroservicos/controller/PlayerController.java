package com.itau.gamemicroservicos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.gamemicroservicos.models.Player;
import com.itau.gamemicroservicos.repositories.PlayerRepository;

@CrossOrigin
@RestController
@RequestMapping("/player")
public class PlayerController {
	
	@Autowired
	PlayerRepository playerCRUD;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> postPlayer(@RequestBody Player player) {
		Player registerPlayer = playerCRUD.save(player);
		return ResponseEntity.status(200).body(registerPlayer);
	}
	
	@RequestMapping(path="/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> getPlayer(@PathVariable int id){
		return ResponseEntity.status(200).body(playerCRUD.findById(id));
	}

}
