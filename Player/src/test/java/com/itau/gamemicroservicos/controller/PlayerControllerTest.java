package com.itau.gamemicroservicos.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.gamemicroservicos.models.Player;
import com.itau.gamemicroservicos.repositories.PlayerRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(PlayerController.class)
public class PlayerControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	PlayerRepository playerCRUD;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void getPlayerTest() throws Exception {
		
		this.mockMvc.perform(get("/player/1")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	
	@Test
	public void postPlayerTest() throws Exception {
				
		Player playerPost = new Player();
		
		playerPost.setIdPlayer(0);
		playerPost.setName("Jogador 3");
		
		String jsonPlayer = mapper.writeValueAsString(playerPost);
		
		when(playerCRUD.save(Mockito.any(Player.class))).thenReturn(playerPost);
		this.mockMvc
				.perform(post("/player")
				.content(jsonPlayer)
				.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			;		
	}
}
