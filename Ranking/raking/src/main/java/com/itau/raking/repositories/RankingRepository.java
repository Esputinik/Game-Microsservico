package com.itau.raking.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.itau.raking.models.Ranking;

public interface RankingRepository extends CrudRepository<Ranking, Long>{

	public Optional<Ranking> findByIdRanking(int idRanking);
	
	@Query(value="SELECT u FROM Ranking u WHERE u.idGame = :idGame ORDER BY u.pontos DESC")
	public Optional<List<Ranking>> findByIdGame(@Param("idGame") int idGame);
	
	@Query(value="SELECT u FROM Ranking u ORDER BY u.pontos DESC")
	public List<Ranking> findAll();
	
}
