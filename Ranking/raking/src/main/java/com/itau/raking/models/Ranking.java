package com.itau.raking.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Ranking {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idRanking;
	
	@NotNull
	//ID do game = número correspondente ao GAME ou ao SUPERGAME.
	private int idGame;
	
	@NotNull
	private int idPlayer;
	
	@NotNull
	//ID do jogo = número correspondente ao jogo finalizado do GAME ou do SUPERGAME.
	private int idJogo;
	
	@NotNull
	private int pontos;
	
	public int getIdRanking() {
		return idRanking;
	}
	public void setIdRanking(int idRanking) {
		this.idRanking = idRanking;
	}
	public int getIdGame() {
		return idGame;
	}
	public void setIdGame(int idGame) {
		this.idGame = idGame;
	}
	public int getIdPlayer() {
		return idPlayer;
	}
	public void setIdPlayer(int idPlayer) {
		this.idPlayer = idPlayer;
	}
	public int getIdJogo() {
		return idJogo;
	}
	public void setIdJogo(int idJogo) {
		this.idJogo = idJogo;
	}
	public int getPontos() {
		return pontos;
	}
	public void setPontos(int pontos) {
		this.pontos = pontos;
	}
	
	

}
