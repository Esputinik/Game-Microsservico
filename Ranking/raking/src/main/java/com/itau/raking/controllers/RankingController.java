package com.itau.raking.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.raking.models.Ranking;
import com.itau.raking.repositories.RankingRepository;

@RestController
@CrossOrigin
@RequestMapping("/ranking")
public class RankingController {

	@Autowired
	RankingRepository rankingRepository;
	
	//Seleciona todos que estão no ranking ordenados por pontos, independente do jogo
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> mostrarAllRanking() {
		return rankingRepository.findAll();
	}
	
	//Seleciona todos que estão no ranking de determinado jogo
	@RequestMapping(method=RequestMethod.GET, path="/{idGame}")
	public ResponseEntity<?> mostrarRankingGame(@PathVariable int idGame){
		Optional<List<Ranking>> optionalRanking = rankingRepository.findByIdGame(idGame);
		if(!optionalRanking.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(optionalRanking);
	}
	
	//Adicionar novo score no ranking
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> addRanking(@Valid @RequestBody Ranking ranking){
		
		if (ranking.getIdGame() == 0 || ranking.getIdJogo() == 0 || ranking.getIdPlayer() == 0) {
			return ResponseEntity.badRequest().build();
		}
		rankingRepository.save(ranking);
		
		return ResponseEntity.ok().body(ranking);
	}
	
	//Atualizar determinado score através do ID do ranking
	@RequestMapping(method=RequestMethod.PUT, path="/{idRanking}")
	public ResponseEntity<?> atualizarRanking(@PathVariable int idRanking, @RequestBody Ranking ranking) {
		Optional<Ranking> optionalRanking = rankingRepository.findByIdRanking(idRanking);
		if(!optionalRanking.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		ranking.setIdRanking(optionalRanking.get().getIdRanking());
		rankingRepository.save(ranking);
		return ResponseEntity.ok().body(optionalRanking);
	}
	
	//Deleta um score do banco através de todos os atributos
	@RequestMapping(method=RequestMethod.DELETE)
	public Ranking removerRanking(@RequestBody Ranking ranking) {
		rankingRepository.delete(ranking);
		return ranking;
	}
	
	//Deleta um score do banco através do idRanking
	@RequestMapping(method=RequestMethod.DELETE, path="/{idRanking}")
	public ResponseEntity<?> removerRankingByIdRanking(@PathVariable int idRanking) {
		Optional<Ranking> optionalRanking = rankingRepository.findByIdRanking(idRanking);
		if(!optionalRanking.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		rankingRepository.delete(optionalRanking.get());
		return ResponseEntity.ok().body(optionalRanking);
	}
	
}
