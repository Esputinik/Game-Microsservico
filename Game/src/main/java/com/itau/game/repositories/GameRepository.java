package com.itau.game.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.game.models.*;

public interface GameRepository extends CrudRepository<Game, Integer> {

	
}
